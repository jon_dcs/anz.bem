from builders.anz_aba_builder import AnzAbaBuilder, Director

def format_linedictdata(d):
    # to integer
    for key in ['job', 'pay_day_of_month', 'pay_amount']:
      if isinstance(key, int):
        d[key] = int(d[key])
      else:
        d[key] = float(d[key])
    return d

def strfile2dictlist(text):
    # to list of rows
    listdata = text.strip().split('\n')
    # grab headerline
    strhead = listdata.pop(0)
    # drop quotes and convert to list.
    # strip removes leading/railing spaces and line ending
    listhead = strhead.strip().replace('"', '').split(',')
    # using header row convert lines to dictionaries - all strings
    ulistofdicts = [dict(zip(listhead, row.replace('"', '').split(','))) for row in listdata]
    # apply particular format for any items in each row
    flistofdicts = []
    for row in ulistofdicts:
        flistofdicts.append(format_linedictdata(row))
    return flistofdicts

testdata = """
"job","account_ref","payee_name","pay_amount","payee_bsb","payee_bank_account","lodgement_ref","pay_day_of_month"
"30862", "780227790","CBA - Loans", 110,"062-000","10267768","780227790","15"
"31378", "4557011020331696","National Australia Bank (NAB)",29,"083-117","827676463","13169772","15"
"31378", "MF31378","HMP Management Fee",137,"","","","15"
"31378", "4557011020010530","National Australia Bank (NAB)",51,"083-117","827676463","13083045","15"
"31486", "CF7242019001673","City Finance",20,"014730","292393044","0413027699","15"
"31486", "4557025690626813","National Australia Bank (NAB)",70,"083-117","827676463","13189471","15"
"31486", "721172750","National Australia Bank (NAB)",299,"083-117","827676463","13188890","15"
"31498", "56125589","Panthera Finance (HMP)",119,"034002","988-345","56125589","15"
"31498", "55879619","Panthera Finance (HMP)",20,"034002","988-345","55879619","15"
"31498", "322291","Mercedes Benz Finance",41,"033864","322291","322291","15"
"""

argasdict = strfile2dictlist(testdata)

run_data = {
  'user_bank': 'ANZ',
  'user_name': 'DCS Group Aust',
  'user_number': '115',
  'user_bsb': '111-555', 
  'user_account': '123',
  'description': 'Dividends',
  'date_submit': '251019'
}

if __name__ == "__main__":
  try:
    director = Director()
    builder = AnzAbaBuilder()
    director.builder = builder
    director.build_full_report(run_data, argasdict)
  except Exception as error:
    print(f'Error: \n{error}\nat row:{builder.data_row}')
  else:
    print("ANZ ABA Report\n--------------\n")
    output = builder.report.combine_parts()

    if __debug__:
      print(f"{output}", end="")
  
