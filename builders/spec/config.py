from __future__ import annotations
import enum

class Alignment(enum.Enum):
  left = 1
  right = 2

class FixedWidthType(enum.Enum):
  string = 1
  integer = 2
  date = 3
  decimal = 4



class FixedWidthConfig:
  def __init__(self, 
    required: bool, 
    type: FixedWidthType, 
    start_pos: int, 
    end_pos: int, 
    alignment: Alignment, 
    padding: str, 
    default: any = None, 
    format: str = None) -> None:
      
    self.required = required
    self.type = type.name
    self.start_pos = start_pos
    self.end_pos = end_pos
    self.alignment = alignment.name
    self.padding = padding
    
    if default:
      self.default = default
    if format:
      self.format = format

  def getProps(self):
    return self.__dict__

class Report():
  """
  It makes sense to use the Builder pattern only when your reports are quite
  complex and require extensive configuration.

  Unlike in other creational patterns, different concrete builders can produce
  unrelated reports. In other words, results of various builders may not
  always follow the same interface.
  """

  def __init__(self) -> None:
    self.parts = []

  def add(self, part: Any) -> None:
    self.parts.append(part)

  def combine_parts(self) -> None:
    makeitastring = ''.join(map(str, self.parts))
    return makeitastring
    