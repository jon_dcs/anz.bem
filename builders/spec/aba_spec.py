from copy import deepcopy
import unittest
import re
from datetime import date, datetime
from spec.config import Alignment, FixedWidthType, FixedWidthConfig
try:
  from fixedwidth import FixedWidth
except ImportError:
  from fixedwidth.fixedwidth import FixedWidth

today = date.today().strftime('%d%m%y')

class AbaDescriptionRecord:
  def __init__(self):
    self.record_type = FixedWidthConfig(False, FixedWidthType.string, 1, 1, Alignment.left, ' ', default='0').getProps()
    self.user_bsb = FixedWidthConfig(True, FixedWidthType.string, 2, 8, Alignment.left, ' ', default=None, format=r'\d{3}-\d{3}').getProps()
    self.user_account = FixedWidthConfig(False, FixedWidthType.string, 9, 17, Alignment.left, ' ').getProps()
    self.reserved_1 = FixedWidthConfig(False, FixedWidthType.string, 18, 18, Alignment.left, ' ').getProps()
    self.sequence_number = FixedWidthConfig(False, FixedWidthType.string, 19, 20, Alignment.left, ' ', default='01').getProps()
    self.user_bank = FixedWidthConfig(False, FixedWidthType.string, 21, 23, Alignment.left, ' ', default='ANZ').getProps()
    self.reserved_2 = FixedWidthConfig(False, FixedWidthType.string, 24, 30, Alignment.left, ' ').getProps()
    self.user_name = FixedWidthConfig(False, FixedWidthType.string, 31, 56, Alignment.left, ' ', default='DCS Group Aust').getProps()
    self.user_number = FixedWidthConfig(True, FixedWidthType.string, 57, 62, Alignment.right, ' ').getProps()
    self.description = FixedWidthConfig(False, FixedWidthType.string, 63, 74, Alignment.left, ' ', default='Dividends').getProps()
    self.date_submit = FixedWidthConfig(False, FixedWidthType.string, 75, 80, Alignment.right, ' ', default=None, format=r'%d%m%y').getProps()
    self.time = FixedWidthConfig(False, FixedWidthType.string, 81, 84, Alignment.right, ' ').getProps()
    self.reserved_3 = FixedWidthConfig(False, FixedWidthType.string, 85, 120, Alignment.left, ' ').getProps()

  def getProps(self) -> dict: 
    return self.__dict__

  def export(self, user_bsb: str, user_account: str, user_number: int, date_submit: str) -> str:
    fw_config: dict = deepcopy(self.getProps())
    fw_obj = FixedWidth(fw_config)
    _user_number = str(user_number).zfill(6)
    fw_obj.update(user_bsb=user_bsb, user_account=user_account, user_number=_user_number, date_submit=date_submit)
    return fw_obj.line
class AbaDetailRecord:
  def __init__(self):
    self.record_type = FixedWidthConfig(False, FixedWidthType.string, 1, 1, Alignment.left, ' ', default='1').getProps()
    self.user_bsb = FixedWidthConfig(True, FixedWidthType.string, 2, 8, Alignment.left, ' ', default=None, format=r'\d{3}-\d{3}').getProps()
    self.user_account = FixedWidthConfig(True, FixedWidthType.string, 9, 17, Alignment.left, ' ').getProps()
    self.tax_indicator = FixedWidthConfig(False, FixedWidthType.string, 18, 18, Alignment.left, ' ').getProps()
    self.code = FixedWidthConfig(False, FixedWidthType.integer, 19, 20, Alignment.left, ' ', default=53).getProps()
    self.amount = FixedWidthConfig(True, FixedWidthType.string, 21, 30, Alignment.right, ' ').getProps()
    self.title = FixedWidthConfig(True, FixedWidthType.string, 31, 62, Alignment.left, ' ').getProps()
    self.lodgement_reference = FixedWidthConfig(False, FixedWidthType.string, 63, 80, Alignment.left, ' ', default='Dividend').getProps()
    self.trace_bsb = FixedWidthConfig(True, FixedWidthType.string, 81, 87, Alignment.left, ' ', default=None, format=r'\d{3}-\d{3}').getProps()
    self.trace_account = FixedWidthConfig(True, FixedWidthType.string, 88, 96, Alignment.left, ' ').getProps()
    self.remitter = FixedWidthConfig(False, FixedWidthType.string, 97, 112, Alignment.left, ' ', default='DCS Group Aust').getProps()
    self.withholding_amount = FixedWidthConfig(True, FixedWidthType.string, 113, 120, Alignment.right, ' ').getProps()

  def getProps(self) -> dict: 
    return self.__dict__

  def export(self, user_bsb: str, user_account: str, amount: float, title: str,
  trace_bsb: str, trace_account: str, withholding_amount: float) -> str:

    fw_config: dict = deepcopy(self.getProps())
    fw_obj = FixedWidth(fw_config)
    _amount = "{0:.2f}".format(amount).zfill(11)
    _amount = _amount.replace(".", "")
    _withholding_amount = "{0:.2f}".format(withholding_amount).zfill(9)
    _withholding_amount = _withholding_amount.replace(".", "")

    fw_obj.update(
      user_bsb=user_bsb, 
      user_account=user_account, 
      amount=_amount, 
      title=title,
      trace_bsb=trace_bsb,
      trace_account=trace_account,
      withholding_amount=_withholding_amount
    )
    return fw_obj.line
class AbaBatchControlRecord:
  def __init__(self):
    self.record_type = FixedWidthConfig(False, FixedWidthType.string, 1, 1, Alignment.left, ' ', default='7').getProps()
    self.reserved_1 = FixedWidthConfig(False, FixedWidthType.string, 2, 8, Alignment.left, ' ', default='999-999').getProps()
    self.reserved_2 = FixedWidthConfig(False, FixedWidthType.string, 9, 20, Alignment.left, ' ').getProps()
    self.total_net = FixedWidthConfig(True, FixedWidthType.string, 21, 30, Alignment.right, ' ').getProps()
    self.total_credit = FixedWidthConfig(True, FixedWidthType.string, 31, 40, Alignment.right, ' ').getProps()
    self.total_debit = FixedWidthConfig(True, FixedWidthType.string, 41, 50, Alignment.right, ' ').getProps()
    self.reserved_3 = FixedWidthConfig(False, FixedWidthType.string, 51, 74, Alignment.left, ' ').getProps()
    self.count = FixedWidthConfig(True, FixedWidthType.string, 75, 80, Alignment.right, ' ').getProps()
    self.reserved_4 = FixedWidthConfig(False, FixedWidthType.string, 81, 120, Alignment.left, ' ').getProps()

  def getProps(self) -> dict: 
    return self.__dict__

  def export(self, total_net: float, total_credit: float, total_debit: float, count: int) -> str:
    fw_config: dict = deepcopy(self.getProps())
    fw_obj = FixedWidth(fw_config)
    _total_net = "{:.2f}".format(total_net).zfill(11)
    _total_net = _total_net.replace(".", "")
    _total_credit = "{:.2f}".format(total_credit).zfill(11)
    _total_credit = _total_credit.replace(".", "")
    _total_debit = "{:.2f}".format(total_debit).zfill(11)
    _total_debit = _total_debit.replace(".", "")
    _count = str(count).zfill(6)
    fw_obj.update(total_net=_total_net, total_credit=_total_credit, total_debit=_total_debit, count=_count)

    return fw_obj.line

class TestFixedWidth(unittest.TestCase):
  def test_description_record(self):
    aba = AbaDescriptionRecord()
    result = aba.export("013-999", "11684", 33, "241019")
    good = (f"0013-999" + "11684".ljust(10) + "01ANZ" + "".ljust(7) + "DCS Group Aust".ljust(26) + "000033" +
    "Dividends".ljust(12) + f"241019" + "".ljust(4) + "".ljust(36) + "\r\n")
    self.assertEqual(result, good)

  def test_detail_record(self):
    aba = AbaDetailRecord()
    result = aba.export("013-999", "0015", 753.99, "SMITH John Alan", "013-999", "0015", 345.95)
    good = (f"1013-999" + "0015".ljust(9) + "".ljust(1) + "530000075399" + "SMITH John Alan".ljust(32) + "Dividend".ljust(18) + 
    "013-999" + "0015".ljust(9) + "DCS Group Aust".ljust(16) + "00034595\r\n")
    self.assertEqual(result, good)

  def test_control_record(self):
    aba = AbaBatchControlRecord()
    result = aba.export(1500.00, 500.00, 1000.00, 88)
    good = (f"7999-999" + "".ljust(12) + "000015000000000500000000100000" + "".ljust(24) + "000088" + "".ljust(40) + "\r\n")
    self.assertEqual(result, good)

if __name__ == '__main__':
  unittest.main()