from copy import deepcopy
import unittest
from datetime import date, datetime
from spec.config import Alignment, FixedWidthType, FixedWidthConfig

try:
  from fixedwidth import FixedWidth
except ImportError:
  from fixedwidth.fixedwidth import FixedWidth

today = date.today().strftime('%Y%m%d')
now = datetime.now().strftime('%H%M%S')

class BemFileHeaderRecord:
  def __init__(self):
    self.record_type = FixedWidthConfig(False, FixedWidthType.string, 1, 2, Alignment.left, ' ', default='00').getProps()
    self.payer_institution_code = FixedWidthConfig(True, FixedWidthType.string, 3, 5, Alignment.left, ' ').getProps()
    self.file_creation_date = FixedWidthConfig(False, FixedWidthType.date, 6, 13, Alignment.right, ' ', default=today, format='%Y%m%d').getProps()
    self.file_creation_time = FixedWidthConfig(False, FixedWidthType.date, 14, 19, Alignment.right, ' ', default=now, format='%H%M%S').getProps()
    self.file_number = FixedWidthConfig(True, FixedWidthType.string, 20, 22, Alignment.right, ' ').getProps()
    self.filler = FixedWidthConfig(False, FixedWidthType.string, 23, 201, Alignment.right, ' ').getProps()

  def getProps(self) -> dict: 
    return self.__dict__

  def export(self, code: str, batch: int) -> str:
    fw_config: dict = deepcopy(self.getProps())
    fw_obj = FixedWidth(fw_config)
    fileNumber = str(batch).zfill(3)
    fw_obj.update(payer_institution_code=code, file_number=fileNumber)

    return fw_obj.line

class BemPaymentInstructionRecord:
  def __init__(self):
    self.record_type = FixedWidthConfig(False, FixedWidthType.string, 1, 2, Alignment.left, ' ', default='50').getProps()
    self.payer_institution_code = FixedWidthConfig(True, FixedWidthType.string, 3, 5, Alignment.left, ' ').getProps()
    self.payment_account_detail = FixedWidthConfig(True, FixedWidthType.string, 6, 25, Alignment.left, ' ').getProps()
    self.country_of_payment = FixedWidthConfig(False, FixedWidthType.string, 26, 28, Alignment.left, ' ', default='AUS').getProps()
    self.state_of_payment = FixedWidthConfig(False, FixedWidthType.string, 29, 31, Alignment.left, ' ', default='QLD').getProps()
    self.currency_code_of_payment = FixedWidthConfig(False, FixedWidthType.string, 32, 34, Alignment.left, ' ', default='AUD').getProps()
    self.biller_code = FixedWidthConfig(True, FixedWidthType.string, 35, 44, Alignment.left, ' ').getProps()
    self.service_code = FixedWidthConfig(False, FixedWidthType.string, 45, 51, Alignment.left, ' ', default='0000000').getProps()
    self.customer_reference_number = FixedWidthConfig(True, FixedWidthType.string, 52, 71, Alignment.left, ' ').getProps()
    self.amount = FixedWidthConfig(True, FixedWidthType.string, 72, 83, Alignment.right, ' ').getProps()
    self.bpay_settle_date = FixedWidthConfig(False, FixedWidthType.string, 84, 91, Alignment.right, ' ').getProps()
    self.payer_name = FixedWidthConfig(False, FixedWidthType.string, 92, 131, Alignment.left, ' ', default='DCS Group Aust').getProps()
    self.additional_reference_code = FixedWidthConfig(False, FixedWidthType.string, 132, 151, Alignment.left, ' ').getProps()
    self.discretionary_data = FixedWidthConfig(False, FixedWidthType.string, 152, 201, Alignment.left, ' ').getProps()

  def getProps(self) -> dict: 
    return self.__dict__

  def export(self, code: str, bsb: str, account: str, biller_code: int, customer_ref: int, amount: float) -> str:
    fw_config: dict = deepcopy(self.getProps())
    fw_obj = FixedWidth(fw_config)
    _amount = "{0:.2f}".format(amount).zfill(13)
    _amount = _amount.replace(".", "")
    _payment_account = (bsb + account).replace('-', "")

    fw_obj.update(payer_institution_code=code, payment_account_detail=_payment_account,
      biller_code = str(biller_code).zfill(10),
      customer_reference_number = str(customer_ref),
      amount = _amount)

    return fw_obj.line

class BemFileTrailerRecord:
  def __init__(self):
    self.record_type = FixedWidthConfig(False, FixedWidthType.string, 1, 2, Alignment.left, ' ', default='99').getProps()
    self.payer_institution_code = FixedWidthConfig(True, FixedWidthType.string, 3, 5, Alignment.left, ' ').getProps()
    self.file_creation_date = FixedWidthConfig(False, FixedWidthType.date, 6, 13, Alignment.right, ' ', default=today, format='%Y%m%d').getProps()
    self.file_creation_time = FixedWidthConfig(False, FixedWidthType.date, 14, 19, Alignment.right, ' ', default=now, format='%H%M%S').getProps()
    self.file_number = FixedWidthConfig(True, FixedWidthType.string, 20, 22, Alignment.right, ' ').getProps()
    self.number_of_payments = FixedWidthConfig(True, FixedWidthType.string, 23, 31, Alignment.right, ' ').getProps()
    self.amount_of_payments = FixedWidthConfig(True, FixedWidthType.string, 32, 46, Alignment.right, ' ').getProps()
    self.filler = FixedWidthConfig(False, FixedWidthType.string, 47, 201, Alignment.right, ' ').getProps()

  def getProps(self) -> dict: 
    return self.__dict__

  def export(self, code: str, batch: str, count: int, total: float) -> str:
    fw_config: dict = deepcopy(self.getProps())
    fw_obj = FixedWidth(fw_config)
    fileNumber = str(batch).zfill(3)
    _amount = "{:.2f}".format(total).zfill(16)
    _amount = _amount.replace(".", "")
    _count = str(count).zfill(9)
    fw_obj.update(payer_institution_code=code, file_number=fileNumber, number_of_payments=_count, amount_of_payments=_amount)

    return fw_obj.line

class TestFixedWidth(unittest.TestCase):
  def test_header_record(self):
    bem = BemFileHeaderRecord()
    result = bem.export("ANZ", 3)
    good = (f"00ANZ{today}{now}003".ljust(201) + "\r\n")
    self.assertEqual(result, good)

  def test_payment_instruction_record(self):
    bem = BemPaymentInstructionRecord()
    result = bem.export("ANZ", "abc-25", 12345, 8866, 74.4)
    good = (f"50ANZ" + "abc-25".ljust(20) + "AUSQLDAUD00000123450000000" + "8866".ljust(20)
      + "000000007440" + "".ljust(8) + "DCS Group Aust".ljust(40) + "".ljust(70) + "\r\n")
    self.assertEqual(result, good)

  def test_footer_record(self):
    bem = BemFileTrailerRecord()
    result = bem.export("ANZ", 2, 15, 1234.56)
    good = (f"99ANZ{today}{now}002000000015000000000123456".ljust(201) + "\r\n")
    self.assertEqual(result, good)

if __name__ == '__main__':
  unittest.main()