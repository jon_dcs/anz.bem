from __future__ import annotations
from typing import List
from builders.base_report_builder import BaseReportBuilder
from spec.bem_spec import BemFileHeaderRecord, BemPaymentInstructionRecord, BemFileTrailerRecord
from spec.config import Report

class AnzBpayBuilder(BaseReportBuilder):
  """
  The Concrete Builder classes follow the Builder interface and provide
  specific implementations of the building steps. Your program may have
  several variations of Builders, implemented differently.
  """
  data_row = None
  
  def __init__(self) -> None:
    """
    A fresh builder instance should contain a blank report object, which is used in further assembly.
    """
    self.reset()

  def reset(self) -> None:
    self._report = Report()

  @property
  def report(self) -> Report:
    """
    Concrete Builders are supposed to provide their own methods for
    retrieving results. That's because various types of builders may create
    entirely different reports that don't follow the same interface.
    Therefore, such methods cannot be declared in the base Builder interface
    (at least in a statically typed programming language).

    Usually, after returning the end result to the client, a builder
    instance is expected to be ready to start producing another report.
    That's why it's a usual practice to call the reset method at the end of
    the `getReport` method body. However, this behavior is not mandatory,
    and you can make your builders wait for an explicit reset call from the
    client code before disposing of the previous result.
    """
    report = self._report
    self.reset()
    return report

  def produce_part_header(self, run_data) -> None:
    header_record = BemFileHeaderRecord()
    self._report.add(header_record.export(run_data['user_bank'], run_data['batch']))

  def produce_part_body(self, run_data, data) -> None:
    payment_record = BemPaymentInstructionRecord()
    for rec in data:
      self.data_row = rec
      self._report.add(payment_record.export(run_data['user_bank'], run_data['user_bsb'], run_data['user_account'], rec[3], rec[0], rec[2]))

  def produce_part_footer(self, run_data, count: int, total: float) -> None:
    trailer_record = BemFileTrailerRecord()
    self._report.add(trailer_record.export(run_data['user_bank'], run_data['batch'], count, total))

class Director:
  """
  The Director is only responsible for executing the building steps in a
  particular sequence. It is helpful when producing reports according to a
  specific order or configuration. Strictly speaking, the Director class is
  optional, since the client can control builders directly.
  """

  def __init__(self) -> None:
    self._builder = None

  @property
  def builder(self) -> Builder:
    return self._builder

  @builder.setter
  def builder(self, builder: Builder) -> None:
    """
    The Director works with any builder instance that the client code passes
    to it. This way, the client code may alter the final type of the newly assembled report.
    """
    self._builder = builder

  """ The Director can construct several report variations using the same building steps.  """
  def build_basic_report(self, run_data) -> None:
    self.builder.produce_part_header(run_data)
    self.builder.produce_part_body()

  def build_full_report(self, run_data, data) -> None:
    count = len(data)
    total: float = sum(map(lambda row: int(row[2]), data))

    self.builder.produce_part_header(run_data)
    self.builder.produce_part_body(run_data, data)
    self.builder.produce_part_footer(run_data, count, total)