from abc import ABC, abstractmethod, abstractproperty

class BaseReportBuilder(ABC):
  """
  The Builder interface specifies methods for creating the different sections of the Report objects.
  """

  @abstractproperty
  def report(self) -> None:
    pass

  @abstractmethod
  def produce_part_header(self) -> None:
    pass

  @abstractmethod
  def produce_part_body(self) -> None:
    pass

  @abstractmethod
  def produce_part_footer(self) -> None:
    pass