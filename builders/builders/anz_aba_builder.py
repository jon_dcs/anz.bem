from __future__ import annotations
from typing import List
from builders.base_report_builder import BaseReportBuilder
from spec.aba_spec import AbaDescriptionRecord, AbaDetailRecord, AbaBatchControlRecord
from spec.config import Report

class AnzAbaBuilder(BaseReportBuilder):
  data_row = None
  
  def __init__(self) -> None:
    self.reset()

  def reset(self) -> None:
    self._report = Report()

  @property
  def report(self) -> Report:
    report = self._report
    self.reset()
    return report

  def produce_part_header(self, run_data) -> None:
    description_record = AbaDescriptionRecord()
    self._report.add(description_record.export(run_data['user_bsb'], run_data['user_account'], run_data['user_number'], run_data['date_submit']))

  def produce_part_body(self, run_data, data) -> None:
    detail_record = AbaDetailRecord()
    for rec in data:
      self.data_row = rec
      self._report.add(detail_record.export(
        rec['payee_bsb'], rec['payee_bank_account'], rec['pay_amount'], rec['payee_name'],
        run_data['user_bsb'], run_data['user_account'], rec['pay_amount']))

  def produce_part_footer(self, count: int, total: float) -> None:
    batch_record = AbaBatchControlRecord()
    self._report.add(batch_record.export(total, total, total, count))

class Director:
  def __init__(self) -> None:
    self._builder = None

  @property
  def builder(self) -> Builder:
    return self._builder

  @builder.setter
  def builder(self, builder: Builder) -> None:
    self._builder = builder

  def build_full_report(self, run_data, data) -> None:
    count = len(data)
    total: float = sum(map(lambda row: int(row['pay_amount']), data))

    self.builder.produce_part_header(run_data)
    self.builder.produce_part_body(run_data, data)
    self.builder.produce_part_footer(count, total)