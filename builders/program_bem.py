from builders.anz_bpay_builder import AnzBpayBuilder, Director

# Preperation
# prepare / get dependancies prior to assembly of report.

bemRunData = {
  'batch': 3,
  'user_bank': 'ANZ',
  'user_number': '115',
  'user_bsb': '111-555', 
  'user_account': '123',
  'date_submit': '251019'
}

hdr = ['Job', 'payment_account_detail', 'amount', 'biller code', 'date of mont']
data = [
  [31549, '10659989', 62, 187229, 15],
  [30862, '4724373501063604', 74.4, 49551, 15],
  [30862, '603071820', 203.3,	49528, 15],
  [30862, '1002003988953998', 322.4, 17772,15],
  [30862,	'5437931002080602', 90.4,	9779, 15],
  [31378,	'46014895764', 65,	575886, 15],
  [31378,	'19054964', 64,	929836, 15],
  [31378,	'DT2516774', 98,	36855, 15]
]

if __name__ == "__main__":
  """
  The report builder required:
  1: Batch -> int
  2: Data - > [[Job, account, amount, biller code, date of mont] ... ]

  Output will be 
  1: if successful -> result = List[str]
  else:
  2: message of failure
  """

  try:
    # Initialise required dependencies
    director = Director()
    builder = AnzBpayBuilder()
    director.builder = builder

    # instruct director, which flavour we want i.e. full, redacted, summary etc.
    director.build_full_report(bemRunData, data)
  except Exception as error:
    # print library error and global variable
    print(f'Error: \n{error}\nat row:{builder.data_row}')
  else:
    # In debug mode ONLY, include the extra prints
    print("ANZ Bpay Report\n--------------\n")
    output = builder.report.combine_parts()

    if __debug__:
      print(f"{output}", end="")
  
  # print("Generate ABA file: ")
  # director.build_full_report()
  # builder.report.list_parts()
  # print("\n")

  # Remember, the Builder pattern can be used without a Director class.
  # print("Generate the ANZ Bpay report: ")
  # builder.produce_part_footer()
  # builder.report.list_parts()
