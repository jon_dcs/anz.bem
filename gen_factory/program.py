# general purpose Object Factory
# Separate Object Creation to Provide Common Interface
# a common initialization interface for each service implementation is not possible or recommended.

# The best approach is to define a new type of object that provides a general interface and is responsible for the creation of a concrete service.
# This new type of object will be called a Builder.
# The Builder object has all the logic to create and initialize a service instance.
# You will implement a Builder object for each of the supported services
import music

config = {
    'spotify_client_key': 'THE_SPOTIFY_CLIENT_KEY',
    'spotify_client_secret': 'THE_SPOTIFY_CLIENT_SECRET',
    'pandora_client_key': 'THE_PANDORA_CLIENT_KEY',
    'pandora_client_secret': 'THE_PANDORA_CLIENT_SECRET',
    'local_music_location': '/usr/data/music'
}

pandora = music.services.get('PANDORA', **config)
pandora.test_connection()

spotify = music.services.get('SPOTIFY', **config)
spotify.test_connection()

local = music.services.get('LOCAL', **config)
local.test_connection()

pandora2 = music.services.get('PANDORA', **config)
print(f'id(pandora) == id(pandora2): {id(pandora) == id(pandora2)}')

spotify2 = music.services.get('SPOTIFY', **config)
print(f'id(spotify) == id(spotify2): {id(spotify) == id(spotify2)}')