from .xml_serializer import XmlSerializer
from .json_serializer import JsonSerializer
from .yaml_serializer import YamlSerializer

class SerializerFactory:
  def __init__(self):
    self._creators = {}

  def register_format(self, format, creator):
    self._creators[format] = creator

  def get_serializer(self, format):
    creator = self._creators.get(format)
    if not creator:
      raise ValueError(format)
    return creator()

class ObjectSerializer:
  def serialize(self, serializable, format):
    serializer = factory.get_serializer(format)
    serializable.serialize(serializer)
    return serializer.to_str()

factory = SerializerFactory()
factory.register_format("JSON", JsonSerializer)
factory.register_format("XML", XmlSerializer)
factory.register_format('YAML', YamlSerializer)
