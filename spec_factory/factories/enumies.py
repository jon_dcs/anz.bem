import enum

class Reports(enum.Enum):
  anz_bem = 1
  abc = 2
  dea = 3

class Formats(enum.Enum):
  JSON = 1
  XML = 2