import yaml
from .json_serializer import JsonSerializer

#  In Python, any object that provides the desired methods or functions is said to implement the interface.
#  The example defines the Serializer interface to be an object that implements the following methods or functions:

class YamlSerializer(JsonSerializer):
  def to_str(self):
    return yaml.dump(self._current_object)
