class Record:
    def __init__(self, id, name, contract):
        self.id = id
        self.name = name
        self.contract = contract

    def serialize(self, serializer):
        serializer.start_object('record', self.id)
        serializer.add_property('name', self.name)
        serializer.add_property('contract', self.contract)