import Models.records as records
import factories.seralizers_factory as seralizers

def program():
  record = records.Record('10', 'Joe Bloe', 'Part 10')
  factory = seralizers.ObjectSerializer()
  
  print(factory.serialize(record, 'XML'))
  print(factory.serialize(record, 'JSON'))
  print(factory.serialize(record, 'YAML'))
      
if __name__ == "__main__":
    program()
